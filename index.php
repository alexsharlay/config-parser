<?php
require_once "library/db/Db.class.php";

$db = new DB();
$check_bd = $db->query("show tables like 'test'"); //Check for tables in the database

if (empty($check_bd)) {
    //Creating a table
    $create_table = $db->query("CREATE TABLE `test` (
                                `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id записи',
                                `type` enum('magento','prestashop') NOT NULL COMMENT 'тип CMS',
                                `name` varchar(255) NOT NULL COMMENT 'имя директории сайта',
                                `db_host` varchar(255) NOT NULL,
                                `db_name` varchar(255) NOT NULL,
                                `db_username` varchar(255) NOT NULL,
                                `db_password` varchar(255) NOT NULL,
                                `date` datetime NOT NULL COMMENT 'время обхода',
                                `last_modified` date NOT NULL COMMENT 'время последнего изменения файла конфига',
                                  PRIMARY KEY (`id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
}

require_once "FileHealper.php";

$fileHealper = new FileHealper('/hosts');

$fileHealper->readConfig();

$configs = $db->query("SELECT * FROM test");
?>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>Парсер файлов конфигурации</title>
</head>
<body>
<table border="1">
    <tr>
        <th>Тип</th>
        <th>Имя</th>
        <th>DB Host</th>
        <th>DB Username</th>
        <th>DB Password</th>
    </tr>
    <?php
    foreach ($configs as $config) {
        ?>
        <tr>

            <td><?php echo $config['type']; ?></td>
            <td><?php echo $config['db_name']; ?></td>
            <td><?php echo $config['db_host']; ?></td>
            <td><?php echo $config['db_username']; ?></td>
            <td><?php echo $config['db_password']; ?></td>

        </tr>
        <?php
    }
    ?>
</table>
</body>
</html>
