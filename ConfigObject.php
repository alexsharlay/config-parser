<?php

/**
 * Class for working with the configuration file
 * Class ConfigObject
 */
class ConfigObject
{

    public $type = '';

    public $last_modified = '';

    public $name = '';

    public $db_host = '';

    public $db_username = '';

    public $db_password = '';

    public $db_name = '';

    public $date = '';

    /**
     * ConfigObject constructor.
     * @param $filePath
     * @param $type
     */
    public function __construct($filePath, $type)
    {
        $this->date = date('Y-m-d H:i:s');
        $this->last_modified = date('Y-m-d H:i:s', filemtime($filePath));
        $this->name = $filePath;
        $this->type = $type;
    }

    /**
     * The function saves or updates the object in the database
     */
    public function save()
    {
        require_once 'library/db/Db.class.php';

        $db = new DB();

        $query = "SELECT id FROM test WHERE db_name = :db_name"; //Checkin if there is an entry in the database
        $result = $db->query($query, ['db_name' => $this->db_name]);

        $params = [
            'type'          => $this->type,
            'name'          => $this->name,
            'db_host'       => $this->db_host,
            'db_name'       => $this->db_name,
            'db_username'   => $this->db_username,
            'db_password'   => $this->db_password,
            'date'          => $this->date,
            'last_modified' => $this->last_modified,
        ];

        if (!empty($this->type) && !empty($this->last_modified) && !empty($this->name) && !empty($this->db_host) &&
            !empty($this->db_username) && !empty($this->db_password) && !empty($this->db_name) && !empty($this->date)
        ) {
            //If there is a configuration entry, then we update the record
            if (isset($result[0]['id']) && !empty($result[0]['id'])) {
                $params['id'] = $result[0]['id'];

                $query = "UPDATE test SET type=:type, name = :name, db_host = :db_host, db_name = :db_name, db_username = :db_username, db_password = :db_password,
                                      date = :date, last_modified = :last_modified WHERE id = :id";

                $update = $db->query($query, $params);
            } else {
                $query = "INSERT INTO test (type, name, db_host, db_name, db_username, db_password, date, last_modified)
                  VALUES (:type, :name, :db_host, :db_name, :db_username, :db_password, :date, :last_modified)";

                $insert = $db->query($query, $params);
            }
        }

    }

}