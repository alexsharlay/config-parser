<?php

/**
 * Class ConfigParser
 */
class ConfigParser
{
    /**
     * The function bypasses the Magento configuration file
     * @param $file
     */
    public function magentoConfigParser($file)
    {
        $reader = new XMLReader();
        $reader->open($file);

        require_once 'ConfigObject.php';
        $configObj = new ConfigObject($file, 'magento'); // The object of the configuration file

        /*
         * Runing the xml file and write the parameters to the object
         */
        while ($reader->read()) {
            if ($reader->localName == 'connection') {
                while ($reader->read()) {
                    if ($reader->nodeType == XMLReader::ELEMENT) {
                        $nodeType = '';
                        switch ($reader->localName) {
                            case 'host':
                                $nodeType = 'db_host';
                                break;
                            case 'username':
                                $nodeType = 'db_username';
                                break;
                            case 'password':
                                $nodeType = 'db_password';
                                break;
                            case 'dbname':
                                $nodeType = 'db_name';
                                break;
                            default:
                                $nodeType = false;
                                break;
                        }
                    }

                    if ($reader->nodeType == XMLReader::CDATA && $nodeType) {
                        $configObj->{$nodeType} = $reader->value;
                    }
                }
            }
        }
        $configObj->save(); // Save or update the file in the database
    }

    /**
     * The function bypasses the Presta shop configuration file
     * @param $file
     */
    public function prestaShopConfigParser($file)
    {
        $paramsArr = [];
        require_once 'ConfigObject.php';
        $configObj = new ConfigObject($file, 'prestashop'); // The object of the configuration file

        /*
         * Runing the configuration file and write to the object
         */
        $params = file($file);

        foreach ($params as $param) {
            preg_match_all('/\'(.*)\'/Ui', $param, $paramsArr[], PREG_PATTERN_ORDER);
        }
        foreach ($paramsArr as $param) {
            switch ($param[1][0]) {
                case '_DB_SERVER_' :
                    $configObj->db_host = $param[1][1];
                    break;
                case '_DB_USER_' :
                    $configObj->db_username = $param[1][1];
                    break;
                case '_DB_NAME_' :
                    $configObj->db_name = $param[1][1];
                    break;
                case '_DB_PASSWD_' :
                    $configObj->db_password = $param[1][1];
                    break;
            }

        }
        $configObj->save(); //Save or update the record in the database
    }
}