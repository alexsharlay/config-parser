<?php
/**
 * Class for finding configuration files
 */
class FileHealper
{
    /**
     * Configuration file name: Magento
     */
    const MAGENTO_CONFIG = 'local.xml';
    /**
     * Configuration file name: Presta shop
     */
    const PRESTA_SHOP_CONFIG = 'settings.inc.php';

    /**
     * Directory with configuration files
     * @var string
     */
    protected $directory = '/hosts';

    /**
     * FileHealper constructor.
     * @param bool $directory
     */
    public function __construct($directory = false)
    {
        if ($directory){
            $this->directory = $_SERVER['DOCUMENT_ROOT'] . $directory;
        }
    }

    /**
     * The function of finding configuration files and starting the parser
     */
    function readConfig()
    {
        $iterator = new RecursiveDirectoryIterator($this->directory);
        foreach (new RecursiveIteratorIterator($iterator) as $file) {
            if ($file->isFile()) {
                require_once 'ConfigParser.php';
                $configParser = new ConfigParser();

                $filePath = $file->getRealPath();

                switch ($file->getFileName()){
                    case self::MAGENTO_CONFIG:
                        $configParser->magentoConfigParser($filePath);
                        break;
                    case self::PRESTA_SHOP_CONFIG:
                        $configParser->prestaShopConfigParser($filePath);
                        break;
                }
            }
        }
    }
}
